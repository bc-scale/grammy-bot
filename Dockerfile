# build stage
FROM node:14.17.3
WORKDIR /src
COPY . ${WORKDIR}
RUN npm install
CMD [ "npm", "run", "-u", "start" ]