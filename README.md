<img src="./assets/lp-logo.png" alt="Ligma Prods" height="60"/>
<img src="https://grammy.dev/Y.png" alt="GrammY" height="60"/>
<img src="https://core.telegram.org/file/811140327/1/zlN4goPTupk/9ff2f2f01c4bd1b013" alt="BotFather" height="60"/>

# GrammY Bot Demo

## Requisitos
- node 14.x o superior
- npm


## Instrucciones

### 1- Instalar dependencias
```
npm install
```

### 2- Registrar Bot en Telegram
- Escribe @BotFather en la barra de búsqueda de Telegram
- Elige la primera opción, que aparece con un “check” rojo de verificación (demuestra que es el bot oficial de Telegram)
- Clica en INICIAR
- Escribe el comando /newbot o haz clic sobre el mismo en la lista de comandos que se muestra en el chat
- Seguidamente, escribe un nombre para tu bot y pulsa enter
- Ahora se te pedirá establecer un nombre de usuario, que en este caso debe terminar en “bot”  
- Guarda el token de autenticación asignado por el sistema, será necesario para que puedas acceder a la API de Telegram Bot  

### 3- Definir .env.development y .env.production
Crea los archivos para variables de entorno desarrollo y producción:
- .env.development
```
NODE_ENV=development
BOT_TOKEN=<el token de tu bot de desarrollo>
```
- .env.production
```
NODE_ENV=production
BOT_TOKEN=<el token de tu bot de produccion>
```
   
### 4- Ejecutar localmente
- Desarrollo
```
npm run dev
```
- Producción
```
npm run start
```

### 5- Despliegue en producción (Requiere docker & docker-compose)
```
docker-compose up --build -d
```


## Enlaces de interés
- https://grammy.dev/
- https://core.telegram.org/api