import { Bot, session } from "grammy";
import { conversations, createConversation } from "@grammyjs/conversations";
import botFunctions from "./controllers/botFunctions.js";
import dataFunctions from "./controllers/dataFunctions.js";
import { loginPrompter } from "./controllers/conversationFunctions.js";
import customEnv from "custom-env";
customEnv.env(process.env.NODE_ENV);

const bot = new Bot(process.env.BOT_TOKEN);

// Bot middleware
bot.use(session({ initial: () => ({}) }));
bot.use(conversations());
bot.use(createConversation(loginPrompter));

// Bot menu
bot.api.setMyCommands([
  { command: "start", description: "Inicia el bot" },
  { command: "subscribe", description: "Registra usuario" },
  { command: "unsubscribe", description: "Elimina usuario del registro" },
  { command: "adminlist", description: "Consulta usuarios registrados" }
]);

// Commands
bot.command("start", (ctx) => botFunctions.welcomeMessage(ctx));
bot.command("subscribe", async (ctx) => { await ctx.conversation.enter("loginPrompter") });
bot.command("unsubscribe", (ctx) => botFunctions.unsubscribe(ctx));
bot.command("adminlist", (ctx) => botFunctions.sendAdminList(ctx));
bot.on("message:text", (ctx) => { botFunctions.replyMessage(ctx) });

// Funcion para mandar mensaje a todos los usuarios suscritos
export async function sendMessageToAll(message, options) {
  try {
    const adminList = await dataFunctions.getAdminList();
    // console.log("🚀 ~ file: bot.js ~ line 185 ~ sendMessageToAll ~ adminList", adminList)
    for (const subscriber of adminList) {
      bot.api.sendMessage(subscriber.id, message, options);
    }
  } catch (error) {
    console.log(e);
  }
};

// Arrancar Bot
bot.start();
