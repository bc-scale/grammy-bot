import dataFunctions from "./dataFunctions.js";

export async function loginPrompter(conversation, ctx) {
  await ctx.reply("Introduce nombre de usuario");
  const loginAnswer = await conversation.wait();
  // console.log(loginAnswer.message.text);
  const userLogin = loginAnswer.message.text;
  await ctx.reply("Introduce contraseña de acceso");
  const passwordAnswer = await conversation.wait();
  // console.log(passwordAnswer);
  const userPass = passwordAnswer.message.text;
  // console.log(userLogin, userPass);
  const resLog = await dataFunctions.saveAdmin(ctx.msg.from);
  ctx.reply(resLog);
};