// import dbService from "../services/dbService/dbService.js";

// Funciones con datos
let fakeDatabase = {
  users: []
};

export default {
  getAdminList() {
    return fakeDatabase.users;
  },

  saveAdmin(userData) {
    const alreadyRegistered = fakeDatabase.users.find(item => item.id === userData.id);
    if (alreadyRegistered) {
      return "El usuario ya está registrado";
    }
    fakeDatabase.users.push({
      id: userData.id,
      username: userData.username
    });
    return `@${userData.username} se ha registrado al canal de notificaciones\nLos mensajes que mandes se repicaran a todos los suscriptores`;
  },

  async deleteAdmin(userData) {
    const index = fakeDatabase.users.findIndex(item => item.id === userData.id);
    if (index === -1) {
      return "El usuario no estaba registrado";
    }
    fakeDatabase.users.splice(index, 1);
    return `@${userData.username} se ha eliminado del registro`;
  }
}